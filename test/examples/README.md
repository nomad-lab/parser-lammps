# Test file description
    
## input.1_methyl_naphthalene

    Example of a LAMMPS input file. Here a MD run is set (force field, integrator, constraints, pair style)

## data.1_methyl_naphthalene

    Example of a LAMMPS data file. Here the system's topology is set.

## log.naph_298_396_20ns

    Example of a LAMMPS log file. Here the output of the MD is recorded.
    
## generated_from_data.xyz

    .xyz file generated from the LAMMPS data file